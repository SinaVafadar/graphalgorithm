cmake_minimum_required(VERSION 3.16)
project(introduction_to_algorithm_phase_2_a)

set(CMAKE_CXX_STANDARD 14)

add_executable(introduction_to_algorithm_phase_2_a main.cpp)