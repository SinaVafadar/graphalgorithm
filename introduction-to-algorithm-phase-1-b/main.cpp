#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <algorithm>
#include <thread>

#define FLOAT_MAX +10000000

float global_distance;
struct Node *nearest_neighbour = nullptr;
struct Node *minimum_father = nullptr;
struct Node *main_tree = nullptr;
struct Node **root_of_tree = &main_tree;
std::vector<struct Edge> edges_queue;
std::vector<struct Edge> MST;
float approximate_distance = 0;
int approximate = 10000;

float global_distance_two;
struct Node *nearest_neighbour_two = nullptr;
struct Node *minimum_father_two = nullptr;
struct Node *main_tree_two = nullptr;
struct Node **root_of_tree_two = &main_tree_two;
std::vector<struct Edge> edges_queue_two;
std::vector<struct Edge> MST_two;
float approximate_distance_two = 0;
int approximate_two = 10000;

struct Node {
    float coordinate[2]{};
    int state{};
    struct Node *left = nullptr;
    struct Node *right = nullptr;
    bool is_fragmented = false;

    Node(float x, float y, int state) {
        this -> coordinate[0] = x;
        this -> coordinate[1] = y;
        this -> state = state;
    }
};

struct Edge {
    struct Node *source = nullptr;
    struct Node *destination = nullptr;
    float distance;

    Edge(struct Node *source, struct Node *destination, float distance) {
        this -> source = source;
        this -> destination = destination;
        this -> distance = distance;
    }
};

float find_distance(float x1, float y1, float x2, float y2) {
    return (float) sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

bool compare(const struct Edge first, const struct Edge second) {
    return first.distance >= second.distance;
}

void insert_node(struct Node **root, struct Node *node, int depth) {
    if (*root == nullptr) {*root = node; return ;}

    int dimension = depth % 2;
    if (node -> coordinate[dimension] < (*root) -> coordinate[dimension]) {
        insert_node(&(*root) -> left, node, depth + 1);
    } else {
        insert_node(&(*root) -> right, node, depth + 1);
    }
}

void nearest_neighbour_search(struct Node *root, struct Node *node, int depth) {
    if (root == nullptr) return ;

    float distance_from_root = find_distance(node -> coordinate[0], node -> coordinate[1],
                                             root -> coordinate[0], root -> coordinate[1]);
    if (distance_from_root < global_distance) {
        nearest_neighbour = root;
        global_distance = distance_from_root;
    }

    int dimension = depth % 2;
    if (node -> coordinate[dimension] < root -> coordinate[dimension]) {
        if (root -> left != nullptr) nearest_neighbour_search(root -> left, node, depth + 1);
    } else {
        if (root -> right != nullptr) nearest_neighbour_search(root -> right, node, depth + 1);
    }

    float worse_side_distance = std::abs(root -> coordinate[dimension] - node -> coordinate[dimension]);
    if (worse_side_distance < global_distance) {
        if (node -> coordinate[dimension] < root -> coordinate[dimension]) {
            if (root -> right != nullptr) nearest_neighbour_search(root -> right, node, depth + 1);
        } else {
            if (root -> left != nullptr) nearest_neighbour_search(root -> left, node, depth + 1);
        }
    }
}


std::pair<struct Node *, struct Node *> find_minimum(struct Node *root, int dimension_of_minimum, int depth, struct Node *father) {
    if (root == nullptr) return std::make_pair(nullptr, nullptr);

    int dimension = depth % 2;
    if (dimension == dimension_of_minimum) {
        if (root -> left == nullptr) return std::make_pair(root, father);
        return find_minimum(root -> left, dimension_of_minimum, depth + 1, root);
    }

    std::pair<struct Node *, struct Node *> left_pair = find_minimum(root -> left, dimension_of_minimum, depth + 1, root);
    struct Node *left = left_pair.first;
    std::pair<struct Node *, struct Node *> right_pair = find_minimum(root -> right, dimension_of_minimum, depth + 1, root);
    struct Node *right = right_pair.first;

    std::pair<struct Node *, struct Node *> temp_pair = right_pair;
    struct Node *temp_node;
    if (left != nullptr && right != nullptr) {
        if (left -> coordinate[dimension_of_minimum] < right -> coordinate[dimension_of_minimum]) temp_pair = left_pair;
    } else if (left == nullptr && right != nullptr) {
        temp_pair = right_pair;
    } else if (left != nullptr) {
        temp_pair = left_pair;
    }
    temp_node = temp_pair.first;

    std::pair<struct Node *, struct Node *> answer_pair = std::make_pair(root, father);
    struct Node *answer = root;
    if (temp_node != nullptr && temp_node -> coordinate[dimension_of_minimum] < answer -> coordinate[dimension_of_minimum]) {
        minimum_father = root;
        answer_pair = temp_pair;
    }

    return answer_pair;
}


struct Node *delete_node(struct Node *root, struct Node *node, int depth, struct Node *father) {
    if (root == nullptr) return root;

    int dimension = depth % 2;
    struct Node *minimum_node;
    std::pair<struct Node *, struct Node *> minimum_pair;
    if (root == node) {
        if (root -> right != nullptr) {
            minimum_pair = find_minimum(root -> right, dimension, depth + 1, root);
            minimum_node = minimum_pair.first;
            minimum_father = minimum_pair.second;

            if (node == *root_of_tree) *root_of_tree = minimum_node;

            auto *temp = (struct Node *) malloc(sizeof(struct Node));
            *temp = Node(minimum_node -> coordinate[0], minimum_node -> coordinate[1], minimum_node -> state);
            temp -> right = minimum_node -> right;
            temp -> left = minimum_node -> left;

            if (minimum_father == nullptr) {
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> left;
                minimum_node -> right = root -> right;
                root -> right = nullptr;
                root -> left = nullptr;

            } else {
                if (minimum_father -> left == minimum_node) minimum_father -> left = temp;
                else if (minimum_father -> right == minimum_node) minimum_father -> right = temp;
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> left;
                minimum_node -> right = root -> right;
                root -> right = nullptr;
                root -> left = nullptr;

            }
            delete_node(minimum_node -> right, temp, depth + 1, minimum_node);

        } else if (root -> left != nullptr) {
            minimum_pair = find_minimum(root -> left, dimension, depth + 1, root);
            minimum_node = minimum_pair.first;
            minimum_father = minimum_pair.second;

            if (node == *root_of_tree) *root_of_tree = minimum_node;

            auto *temp = (struct Node *) malloc(sizeof(struct Node));
            *temp = Node(minimum_node -> coordinate[0], minimum_node -> coordinate[1], minimum_node -> state);
            temp -> right = minimum_node -> right;
            temp -> left = minimum_node -> left;

            if (minimum_father == nullptr) {
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> right;
                minimum_node -> right = root -> left;
                root -> right = nullptr;
                root -> left = nullptr;
            } else {
                if (minimum_father -> left == minimum_node) minimum_father -> left = temp;
                else if (minimum_father -> right == minimum_node) minimum_father -> right = temp;
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> right;
                minimum_node -> right = root -> left;
                root -> right = nullptr;
                root -> left = nullptr;
            }
            delete_node(minimum_node -> right, temp, depth + 1, minimum_node);
        } else {
            if (father != nullptr) {
                if (father -> left == node) father -> left = nullptr;
                else if (father -> right == node) father -> right = nullptr;
            }
            return nullptr;
        }

        return minimum_node;
    }

    if (node -> coordinate[dimension] < root -> coordinate[dimension]) delete_node(root -> left, node, depth + 1, root);
    else delete_node(root -> right, node, depth + 1, root);

    return root;
}

void nearest_neighbour_search_two(struct Node *root, struct Node *node, int depth) {
    if (root == nullptr) return ;

    float distance_from_root = find_distance(node -> coordinate[0], node -> coordinate[1],
                                             root -> coordinate[0], root -> coordinate[1]);
    if (distance_from_root < global_distance_two) {
        nearest_neighbour_two = root;
        global_distance_two = distance_from_root;
    }

    int dimension = depth % 2;
    if (node -> coordinate[dimension] < root -> coordinate[dimension]) {
        if (root -> left != nullptr) nearest_neighbour_search_two(root -> left, node, depth + 1);
    } else {
        if (root -> right != nullptr) nearest_neighbour_search_two(root -> right, node, depth + 1);
    }

    float worse_side_distance = std::abs(root -> coordinate[dimension] - node -> coordinate[dimension]);
    if (worse_side_distance < global_distance_two) {
        if (node -> coordinate[dimension] < root -> coordinate[dimension]) {
            if (root -> right != nullptr) nearest_neighbour_search_two(root -> right, node, depth + 1);
        } else {
            if (root -> left != nullptr) nearest_neighbour_search_two(root -> left, node, depth + 1);
        }
    }
}


std::pair<struct Node *, struct Node *> find_minimum_two(struct Node *root, int dimension_of_minimum, int depth, struct Node *father) {
    if (root == nullptr) return std::make_pair(nullptr, nullptr);

    int dimension = depth % 2;
    if (dimension == dimension_of_minimum) {
        if (root -> left == nullptr) return std::make_pair(root, father);
        return find_minimum_two(root -> left, dimension_of_minimum, depth + 1, root);
    }

    std::pair<struct Node *, struct Node *> left_pair = find_minimum_two(root -> left, dimension_of_minimum, depth + 1, root);
    struct Node *left = left_pair.first;
    std::pair<struct Node *, struct Node *> right_pair = find_minimum_two(root -> right, dimension_of_minimum, depth + 1, root);
    struct Node *right = right_pair.first;

    std::pair<struct Node *, struct Node *> temp_pair = right_pair;
    struct Node *temp_node;
    if (left != nullptr && right != nullptr) {
        if (left -> coordinate[dimension_of_minimum] < right -> coordinate[dimension_of_minimum]) temp_pair = left_pair;
    } else if (left == nullptr && right != nullptr) {
        temp_pair = right_pair;
    } else if (left != nullptr) {
        temp_pair = left_pair;
    }
    temp_node = temp_pair.first;

    std::pair<struct Node *, struct Node *> answer_pair = std::make_pair(root, father);
    struct Node *answer = root;
    if (temp_node != nullptr && temp_node -> coordinate[dimension_of_minimum] < answer -> coordinate[dimension_of_minimum]) {
        minimum_father_two = root;
        answer_pair = temp_pair;
    }

    return answer_pair;
}


struct Node *delete_node_two(struct Node *root, struct Node *node, int depth, struct Node *father) {
    if (root == nullptr) return root;

    int dimension = depth % 2;
    struct Node *minimum_node;
    std::pair<struct Node *, struct Node *> minimum_pair;
    if (root == node) {
        if (root -> right != nullptr) {
            minimum_pair = find_minimum_two(root -> right, dimension, depth + 1, root);
            minimum_node = minimum_pair.first;
            minimum_father_two = minimum_pair.second;

            if (node == *root_of_tree_two) *root_of_tree_two = minimum_node;

            auto *temp = (struct Node *) malloc(sizeof(struct Node));
            *temp = Node(minimum_node -> coordinate[0], minimum_node -> coordinate[1], minimum_node -> state);
            temp -> right = minimum_node -> right;
            temp -> left = minimum_node -> left;

            if (minimum_father_two == nullptr) {
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> left;
                minimum_node -> right = root -> right;
                root -> right = nullptr;
                root -> left = nullptr;

            } else {
                if (minimum_father_two -> left == minimum_node) minimum_father_two -> left = temp;
                else if (minimum_father_two -> right == minimum_node) minimum_father_two -> right = temp;
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> left;
                minimum_node -> right = root -> right;
                root -> right = nullptr;
                root -> left = nullptr;

            }
            delete_node_two(minimum_node -> right, temp, depth + 1, minimum_node);

        } else if (root -> left != nullptr) {
            minimum_pair = find_minimum_two(root -> left, dimension, depth + 1, root);
            minimum_node = minimum_pair.first;
            minimum_father_two = minimum_pair.second;

            if (node == *root_of_tree_two) *root_of_tree_two = minimum_node;

            auto *temp = (struct Node *) malloc(sizeof(struct Node));
            *temp = Node(minimum_node -> coordinate[0], minimum_node -> coordinate[1], minimum_node -> state);
            temp -> right = minimum_node -> right;
            temp -> left = minimum_node -> left;

            if (minimum_father_two == nullptr) {
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> right;
                minimum_node -> right = root -> left;
                root -> right = nullptr;
                root -> left = nullptr;
            } else {
                if (minimum_father_two -> left == minimum_node) minimum_father_two -> left = temp;
                else if (minimum_father_two -> right == minimum_node) minimum_father_two -> right = temp;
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> right;
                minimum_node -> right = root -> left;
                root -> right = nullptr;
                root -> left = nullptr;
            }
            delete_node_two(minimum_node -> right, temp, depth + 1, minimum_node);
        } else {
            if (father != nullptr) {
                if (father -> left == node) father -> left = nullptr;
                else if (father -> right == node) father -> right = nullptr;
            }
            return nullptr;
        }

        return minimum_node;
    }

    if (node -> coordinate[dimension] < root -> coordinate[dimension]) delete_node_two(root -> left, node, depth + 1, root);
    else delete_node_two(root -> right, node, depth + 1, root);

    return root;
}

int approximate_MST_one(struct Node *root, struct Node *father_of_root, int depth) {
    if (root -> left == nullptr && root -> right == nullptr) return 1;

    int left = 0, right = 0;
    if (root -> left != nullptr) {
        left = approximate_MST_one(root -> left, root, depth + 1);
    } if (root -> right != nullptr) {
        right = approximate_MST_one(root -> right, root, depth + 1);
    }

    if (left + right + 1 < approximate) return left + right + 1;

    root_of_tree = &root;
    struct Node *first_node = root;
    first_node -> is_fragmented = true;
    delete_node(root, first_node, depth, nullptr);
    global_distance = FLOAT_MAX;
    nearest_neighbour_search(root, first_node, depth);
    struct Edge new_edge = Edge(first_node, nearest_neighbour, global_distance);
    edges_queue.push_back(new_edge);
    make_heap(edges_queue.begin(), edges_queue.end(), compare);

    int total_edges = left + right;

    while (MST.size() != total_edges) {
        struct Edge candidate_edge = edges_queue.front();
        while (candidate_edge.destination -> is_fragmented) {
            struct Node *source_node = candidate_edge.source;
            pop_heap(edges_queue.begin(), edges_queue.end(), compare);
            edges_queue.pop_back();
            global_distance = FLOAT_MAX;
            nearest_neighbour_search(root, source_node, depth);
            edges_queue.emplace_back(Edge(source_node, nearest_neighbour, global_distance));
            push_heap(edges_queue.begin(), edges_queue.end(), compare);
            candidate_edge = edges_queue.front();
        }
        struct Node *new_node_in_fragment = candidate_edge.destination;
        new_node_in_fragment -> is_fragmented = true;
        MST.push_back(candidate_edge);
        approximate_distance_two = candidate_edge.distance + approximate_distance_two;
        delete_node(root, new_node_in_fragment, depth, nullptr);
        global_distance = FLOAT_MAX;
        nearest_neighbour_search(root, new_node_in_fragment, depth);
        edges_queue.emplace_back(Edge(new_node_in_fragment, nearest_neighbour, global_distance));
        push_heap(edges_queue.begin(), edges_queue.end(), compare);
    }

    edges_queue.clear();
    MST.clear();

    if (father_of_root != nullptr) {
        if (father_of_root -> right != nullptr) {
            father_of_root -> right -> is_fragmented = false;
        } if (father_of_root -> left != nullptr) {
            father_of_root -> left -> is_fragmented = false;
        }
    }

    return 1;
}

int approximate_MST_two(struct Node *root, struct Node *father_of_root, int depth) {
    if (root->left == nullptr && root->right == nullptr) return 1;

    int left = 0, right = 0;
    if (root -> left != nullptr) {
        left = approximate_MST_two(root -> left, root, depth + 1);
    } if (root -> right != nullptr) {
        right = approximate_MST_two(root -> right, root, depth + 1);
    }

    if (left + right + 1 < approximate_two) return left + right + 1;

    root_of_tree_two = &root;
    struct Node *first_node = root;

    first_node -> is_fragmented = true;
    delete_node_two(root, first_node, depth, nullptr);
    global_distance_two = FLOAT_MAX;
    nearest_neighbour_search_two(root, first_node, depth);
    edges_queue_two.emplace_back(Edge(first_node, nearest_neighbour_two, global_distance_two));
    make_heap(edges_queue_two.begin(), edges_queue_two.end(), compare);

    int total_edges = left + right;

    while (MST_two.size() != total_edges) {
        struct Edge candidate_edge = edges_queue_two.front();
        while (candidate_edge.destination -> is_fragmented) {
            struct Node *source_node = candidate_edge.source;
            pop_heap(edges_queue_two.begin(), edges_queue_two.end(), compare);
            edges_queue_two.pop_back();
            global_distance_two = FLOAT_MAX;
            nearest_neighbour_search_two(root, source_node, depth);
            edges_queue_two.emplace_back(Edge(source_node, nearest_neighbour_two, global_distance_two));
            push_heap(edges_queue_two.begin(), edges_queue_two.end(), compare);
            candidate_edge = edges_queue_two.front();
        }
        struct Node *new_node_in_fragment = candidate_edge.destination;
        new_node_in_fragment -> is_fragmented = true;
        MST_two.push_back(candidate_edge);
        approximate_distance_two = candidate_edge.distance + approximate_distance_two;
        delete_node_two(root, new_node_in_fragment, depth, nullptr);
        global_distance_two = FLOAT_MAX;
        nearest_neighbour_search_two(root, new_node_in_fragment, depth);
        edges_queue_two.emplace_back(Edge(new_node_in_fragment, nearest_neighbour_two, global_distance_two));
        push_heap(edges_queue_two.begin(), edges_queue_two.end(), compare);
    }

    edges_queue_two.clear();
    MST_two.clear();

    if (father_of_root != nullptr) {
        if (father_of_root -> right != nullptr) {
            father_of_root -> right->is_fragmented = false;
        } if (father_of_root -> left != nullptr) {
            father_of_root -> left -> is_fragmented = false;
        }
    }

    return 1;
}


int main() {

    clock_t tStart = clock();

    float x;
    float y;
    int state;
    int count;
    std::ifstream infile("/home/sina/Projects/introduction-to-algorithm-phase-1/dataset3.txt");
    infile >> count;

    infile >> x;
    infile >> y;
    infile >> state;
    auto *first_node = (struct Node *) malloc(sizeof(struct Node));
    *first_node = Node(x, y, state);
    first_node -> is_fragmented = true;

    for (int counter = 0; counter < count - 1; counter += 1) {
        infile >> x;
        infile >> y;
        infile >> state;
        auto *new_node = (struct Node *) malloc(sizeof(struct Node));
        *new_node = Node(x, y, state);
        insert_node(&main_tree_two, new_node, 0);
    }

    std::thread thread_one(approximate_MST_one, main_tree_two -> left, nullptr, 1);
    std::thread thread_two(approximate_MST_two, main_tree_two -> right, nullptr, 1);

    float distance_one = find_distance(main_tree_two -> coordinate[0], main_tree_two -> coordinate[1],
                                       main_tree_two -> left->coordinate[0], main_tree_two -> left->coordinate[1]);
    float distance_two = find_distance(main_tree_two -> coordinate[0], main_tree_two -> coordinate[1],
                                       main_tree_two -> right->coordinate[0],main_tree_two -> right->coordinate[1]);

    thread_one.join();
    thread_two.join();

    float total_distance = approximate_distance + approximate_distance_two + distance_one + distance_two;
    std::cout << "Total Distance: " << total_distance << std::endl;
    std::cout << "Total Time: " << (float) (clock() - tStart) / CLOCKS_PER_SEC << std::endl;

    return 0;
}
