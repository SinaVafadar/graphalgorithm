#include <iostream>
#include <vector>
#include <list>
#include <utility>
#include <iomanip>
#include <cmath>

using std::cout;
using std::endl;

struct HeapNode {
    int key;
    int element;
    HeapNode(): key(0), element(0) {};
    HeapNode(int element, int key): key(key), element(element) {};
};

class BinaryHeap {
private:
    std::vector<HeapNode> heap;
    static void swap(struct HeapNode &first, struct HeapNode &second);
    int find_position(int node);
    static int get_parent_node(int node);

public:
    BinaryHeap() {heap.resize(1);};
    explicit BinaryHeap(int number_of_nodes) {heap.resize(number_of_nodes + 1);};
    void min_heapify(int node, int length);
    void build_min_heap(std::vector <int> array);
    void decrease_key(int node, int new_key);
    int extract_min();
    bool is_heap_empty();
};

void BinaryHeap::swap(HeapNode &first, HeapNode &second) {
    struct HeapNode temp_node = first;
    first = second;
    second = temp_node;
}

int BinaryHeap::find_position(int node) {
    int node_index = 0;
    for (int node_counter = 0; node_counter < heap.size(); node_counter += 1) {
        if (heap[node_counter].element == node) {
            node_index = node_counter;
        }
    }
    return node_index;
}

int BinaryHeap::get_parent_node(int node) {
    return std::floor(node / 2);
}

void BinaryHeap::min_heapify(int node, int length) {
    int left = 2 * node;
    int right = 2 * node + 1;
    int smallest;
    if (left <= length && heap[left].key < heap[node].key)
        smallest = left;
    else
        smallest = node;
    if (right <= length && heap[right].key < heap[smallest].key)
        smallest = right;
    if (smallest != node) {
        swap(heap[smallest], heap[node]);
        min_heapify(smallest, length);
    }
}

void BinaryHeap::build_min_heap(std::vector<int> array) {
    for (int node_counter = 0; node_counter < array.size(); node_counter += 1) {
        heap[node_counter + 1].element = node_counter;
        heap[node_counter + 1].key = array[node_counter];
    }
    for (int node_counter = (int) heap.size() / 2; node_counter >= 1; node_counter -= 1) {
        min_heapify(node_counter, (int) heap.size() - 1);
    }
}

void BinaryHeap::decrease_key(int node, int new_key) {
    int node_index = find_position(node);
    if (new_key >= heap[node_index].key) {
        std::cout << "new key is not smaller than current key\n";
        return;
    }
    heap[node_index].key = new_key;
    while (node_index > 1 && heap[get_parent_node(node_index)].key > heap[node_index].key) {
        swap(heap[node_index], heap[get_parent_node(node_index)]);
        node_index = get_parent_node(node_index);
    }
}

int BinaryHeap::extract_min() {
    if (is_heap_empty()) {
        std::cout << "error: heap is empty\n";
        exit(-1);
    }
    int minimum = heap[1].element;
    heap[1] = heap[heap.size() - 1];
    heap.erase(heap.begin() + heap.size() - 1);
    min_heapify(1, (int) heap.size());
    return minimum;
}


bool BinaryHeap::is_heap_empty() {
    return heap.size() <= 1;
}

static const int MAX_DISTANCE = 1000;

class MinimumSpanningTree {
private:
    int number_of_vertices;
    std::vector<std::list<std::pair<int,int>>> adjacency_list;
    std::vector<int> predecessor, distance;
    std::vector<bool> is_visited;
    void initialize_single_source(int start);
    void print_data_array(const std::vector<int>& array) const;
public:
    MinimumSpanningTree():number_of_vertices(0){};
    explicit MinimumSpanningTree(int n):number_of_vertices(n){
        adjacency_list.resize(number_of_vertices);
    }
    void add_edge(int from, int to, int weight);
    void prim_min_queue(int start);

    friend class BinaryHeap;
};

void MinimumSpanningTree::initialize_single_source(int start) {
    distance.resize(number_of_vertices);
    predecessor.resize(number_of_vertices);
    for (int counter = 0; counter < number_of_vertices; counter += 1) {
        distance[counter] = MAX_DISTANCE;
        predecessor[counter] = -1;
    }
    distance[start] = 0;
}

void MinimumSpanningTree::print_data_array(const std::vector<int>& array) const {
    for (int counter = 0; counter < number_of_vertices; counter += 1){
        cout << std::setw(4) << counter;
    }
    std::cout << endl;
    for (int counter = 0; counter < number_of_vertices; counter += 1){
        cout << std::setw(4) << array[counter];
    }
    cout << endl << endl;
}

void MinimumSpanningTree::add_edge(int from, int to, int weight) {
    adjacency_list[from].push_back(std::make_pair(to, weight));
}

void MinimumSpanningTree::prim_min_queue(int start) {
    initialize_single_source(start);
    BinaryHeap min_queue(number_of_vertices);
    min_queue.build_min_heap(distance);
    is_visited.resize(number_of_vertices, false);
    int counter = 0;
    while (!min_queue.is_heap_empty()) {
        int source = min_queue.extract_min();
        is_visited[source] = true;
        for (auto iterator = adjacency_list[source].begin(); iterator != adjacency_list[source].end(); iterator ++) {
            if (!is_visited[iterator -> first] && iterator -> second < distance[iterator -> first]) {
                distance[iterator -> first] = iterator -> second;
                predecessor[iterator -> first] = source;
                min_queue.decrease_key(iterator -> first, distance[iterator -> first]);
                counter += 1;
                cout << counter << endl;
            }
        }
    }
    cout << "print predecessor[]:\n";
    print_data_array(predecessor);
    cout << "print distance[]:\n";
    print_data_array(distance);
    cout << std::setw(3) << "v1" << " - " << std::setw(3) << "v2"<< " : weight\n";
    int vertex_counter = (start + 1) % number_of_vertices;
    while (vertex_counter != start) {
        cout << std::setw(3) << predecessor[vertex_counter] << " - " << std::setw(3) << vertex_counter
             << " : " << std::setw(3) << distance[vertex_counter] <<"\n";
        vertex_counter += 1;
        vertex_counter = vertex_counter % number_of_vertices;
    }
}

class CityToGraph {
public:
    std::vector<std::array<int, 4>> vertices;
    std::vector<std::vector<std::pair<int, int>>> graph;
    void create_complete_graph();
};

void CityToGraph::create_complete_graph() {
    double distance;
    std::pair<int, int> destination_vertex;
    std::vector<std::pair<int, int>> destination_vertices;
    for (int source_index = 0; source_index < vertices.size(); source_index += 1) {
        for (int destination_index = 0; destination_index < vertices.size(); destination_index += 1) {
            if (destination_index != source_index) {
                distance = pow(vertices[source_index][1] - vertices[destination_index][1], 2);
                distance += pow(vertices[source_index][2] - vertices[destination_index][2], 2);
                destination_vertex.first = destination_index;
                destination_vertex.second = (int) distance;
                destination_vertices.emplace_back(destination_vertex);
            }
        }
        graph.emplace_back(destination_vertices);
        destination_vertices.clear();
    }
}

int main() {

    std::vector<std::array<int, 4>> cities;
    for (int counter = 0; counter < 10000; counter += 1) {
        std::array<int, 4> this_array = {counter, counter, counter, counter};
        cities.emplace_back(this_array);
    }

    CityToGraph city_to_graph;
    city_to_graph.vertices = cities;
    city_to_graph.create_complete_graph();

    MinimumSpanningTree minimum_spanning_tree(10000);

    std::vector<std::vector<std::pair<int, int>>> this_graph = city_to_graph.graph;
    for (int counter = 0; counter < 10000; counter += 1) {
        for (int inner_counter = 0; inner_counter < 9999; inner_counter += 1) {
            minimum_spanning_tree.add_edge(counter, this_graph[counter][inner_counter].first, this_graph[counter][inner_counter].second);
        }
    }

    minimum_spanning_tree.prim_min_queue(3);

    return 0;
}
