#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <ctime>
#include <fstream>

using namespace std;

vector<pair<pair <float, float>, pair <float, float>>> minimum_spanning_tree;
vector<pair<float, float>> isolated_nodes;
vector<pair<float, pair<pair <float, float>, pair <float, float>>>> queue;

struct greater_heap {
    bool operator() (const pair<float, pair<pair <float, float>, pair <float, float>>> &first,
                     const pair<float, pair<pair <float, float>, pair <float, float>>> &second) const {
        return first.first > second.first;
    }
};

bool while_test(pair <float, float> top_node) {
    return find(isolated_nodes.begin(), isolated_nodes.end(), top_node) == isolated_nodes.end();
}

int main() {
    clock_t tStart = clock();

    pair<float, pair<pair<float, float>, pair<float, float>>> distance_with_nodes;

    float x;
    float y;
    int state;
    int count;
    ifstream infile("/home/sina/Projects/graphalgorithm/data.txt");
    infile >> count;

    int counter = 0;

    while (counter != count) {
        counter += 1;
        infile >> x;
        infile >> y;
        infile >> state;
        isolated_nodes.emplace_back(make_pair(x, y));
    }



    pair<float, float> top_node = isolated_nodes.front();
    auto iterator = isolated_nodes.begin();
    isolated_nodes.erase(iterator);

    float global_distance = numeric_limits<float>::infinity();
    float this_distance;
    pair<float, float> nearest_node;
    for (auto & isolated_node : isolated_nodes) {
        this_distance = pow(isolated_node.first - top_node.first, 2);
        this_distance += pow(isolated_node.second - top_node.second, 2);
        if (this_distance < global_distance) {
            nearest_node = isolated_node;
            global_distance = this_distance;
        }
    }

    queue.emplace_back(make_pair(global_distance, make_pair(top_node, nearest_node)));
    make_heap(queue.begin(), queue.end(), greater_heap());

    while (minimum_spanning_tree.size() != count - 1) {
        top_node = queue.front().second.second;
        while (while_test(top_node)) {
            distance_with_nodes = queue.front();
            pop_heap(queue.begin(), queue.end(), greater_heap());
            queue.pop_back();
            top_node = distance_with_nodes.second.first;
            global_distance = numeric_limits<float>::infinity();

            for (int i = 0; i < isolated_nodes.size(); i += 1) {
                pair<float, float> isolated_node = isolated_nodes[i];
                this_distance = pow(isolated_node.first - top_node.first, 2);
                this_distance += pow(isolated_node.second - top_node.second, 2);
                if (this_distance < global_distance) {
                    nearest_node = isolated_node;
                    global_distance = this_distance;
                }
            }
            queue.emplace_back(make_pair(global_distance, make_pair(top_node, nearest_node)));
            push_heap(queue.begin(), queue.end(), greater_heap());
            top_node = queue.front().second.second;
        }
        distance_with_nodes = queue.front();
        minimum_spanning_tree.emplace_back(
                make_pair(distance_with_nodes.second.first, distance_with_nodes.second.second));
        top_node = distance_with_nodes.second.second;
        iterator = find(isolated_nodes.begin(), isolated_nodes.end(), top_node);
        isolated_nodes.erase(iterator);
        global_distance = numeric_limits<float>::infinity();
        for (int i = 0; i < isolated_nodes.size(); i += 1) {
            pair<float, float> isolated_node = isolated_nodes[i];
            this_distance = pow(isolated_node.first - top_node.first, 2);
            this_distance += pow(isolated_node.second - top_node.second, 2);
            if (this_distance < global_distance) {
                nearest_node = isolated_node;
                global_distance = this_distance;
            }
        }
        queue.emplace_back(make_pair(global_distance, make_pair(top_node, nearest_node)));
        make_heap(queue.begin(), queue.end(), greater_heap());
    }

    printf("Time taken: %.2fs\n", (float)(clock() - tStart)/CLOCKS_PER_SEC);

    float total_weight = 0;
    for (auto &this_pair : minimum_spanning_tree) {
        total_weight += (float) sqrt(pow(this_pair.first.first - this_pair.second.first, 2) + pow(this_pair.first.second - this_pair.second.second, 2));
    }
    cout << total_weight << endl;

    return 0;
}