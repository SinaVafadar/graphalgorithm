#include <cmath>
#include <vector>
#include <fstream>
#include <iostream>
#include <limits>
#include <algorithm>

const float dx[] = {-1, 0, 1,  0};
const float dy[] = { 0, 1, 0, -1};
const float epsilon = 0.0000001;
std::vector<struct Grid *> all_nodes;
std::vector<int> grid_numbers;

struct Node {
    float coordinate[2]{};
    int state{};
    int number;

    Node(float x, float y, int state, int number) {
        this -> coordinate[0] = x;
        this -> coordinate[1] = y;
        this -> state = state;
        this -> number = number;
    }
};

struct Grid {
    int number;
    std::vector<struct Node> nodes;
    float minimum{};
    float maximum{};
    struct Node capital = Node(0, 0, 0, -1);

    explicit Grid(int number) {
        this -> number = number;
    }
};

float find_all_distances(float x, float y, int count, int grid_number) {
    float final_distance = 0;
    for (int counter = 0; counter <= count; counter += 1) {
        float x_difference = all_nodes[grid_number] -> nodes[counter].coordinate[0] - x;
        float y_difference = all_nodes[grid_number] -> nodes[counter].coordinate[1] - y;
        final_distance += (float) sqrt(pow(x_difference, 2) + pow(y_difference, 2));
    }
    return final_distance;
}

float find_distance(float x1, float y1, float x2, float y2) {
    return (float) sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

int main() {

    clock_t tStart = clock();

    float x;
    float y;
    int state;
    int count;
    struct Grid *new_grid = nullptr;
    std::ifstream infile("/home/sina/Projects/introduction-to-algorithm-phase-1-c/dataset0.txt");
    infile >> count;
    for (int counter = 0; counter < count; counter += 1) {
        infile >> x;
        infile >> y;
        infile >> state;
        grid_numbers.emplace_back(state);
    }

    sort(grid_numbers.begin(), grid_numbers.end());
    grid_numbers.erase(std::unique(grid_numbers.begin(), grid_numbers.end()), grid_numbers.end());
    infile.clear();
    infile.seekg(0, std::ios::beg);

    infile >> count;
    for (int counter = 0; counter < grid_numbers.size(); counter += 1) {
        new_grid = new Grid(counter);
        all_nodes.emplace_back(new_grid);
    }

    for (int counter = 0; counter < count; counter += 1) {
        infile >> x;
        infile >> y;
        infile >> state;
        struct Node new_node = Node(x, y, state, counter + 1);
        new_grid = new Grid(state);
        if (!all_nodes[state - 1] -> nodes.empty()) {
            new_grid = all_nodes[state - 1];
            new_grid -> nodes.emplace_back(new_node);
            new_grid -> capital.coordinate[0] += x;
            new_grid -> capital.coordinate[1] += y;
            if (std::min(x, y) < new_grid -> minimum) new_grid -> minimum = std::min(x, y);
            if (std::max(x, y) > new_grid -> maximum) new_grid -> maximum = std::max(x, y);
        } else {
            new_grid -> nodes.emplace_back(new_node);
            new_grid -> maximum = std::max(x, y);
            new_grid -> minimum = std::min(x, y);
            new_grid -> capital.state = state;
            new_grid -> capital.coordinate[0] = x;
            new_grid -> capital.coordinate[1] = y;
            all_nodes[state - 1] = new_grid;
        }
    }


    for (auto &grid : all_nodes) {
        grid -> capital.coordinate[0] /= grid -> nodes.size();
        grid -> capital.coordinate[1] /= grid -> nodes.size();
    }

    for (auto &grid : all_nodes){
        bool is_done;
        float step = grid -> maximum - grid -> minimum;
        float best_distance = find_all_distances(grid -> capital.coordinate[0], grid -> capital.coordinate[1], grid -> nodes.size(), grid -> number - 1);
        while (step > epsilon) {
            is_done = false;
            for (int side = 0; side < 4; side += 1) {
                float x_neighbour = (float) grid -> capital.coordinate[0] + step * dx[side];
                float y_neighbour = (float) grid -> capital.coordinate[1] + step * dy[side];
                float current_distance = find_all_distances(x_neighbour, y_neighbour, grid -> nodes.size(), grid -> number - 1);
                if (current_distance < best_distance) {
                    best_distance = current_distance;
                    grid -> capital.coordinate[0] = x_neighbour;
                    grid -> capital.coordinate[1] = y_neighbour;
                    is_done = true;
                    break;
                }
            }
            if (!is_done) {
                step = step / 2;
            }
        }
    }

    for (auto &grid : all_nodes) {
        auto best_node = (struct Node *) malloc(sizeof(struct Node));
        *best_node = Node(0, 0, 0, 0);
        float global_distance = std::numeric_limits<float>::infinity();
        for (auto &node : grid -> nodes) {
            float current_distance = find_distance(grid -> capital.coordinate[0], grid -> capital.coordinate[1],
                                                   node.coordinate[0], node.coordinate[1]);
            if (current_distance < global_distance) {
                *best_node = node;
                global_distance = current_distance;
            }
        }
        grid -> capital = *best_node;
    }

    for (auto &grid : all_nodes) {
        std::cout << grid -> number << ": " << grid -> capital.number << std::endl;
    }

    std::ofstream outfile;
    outfile.open("/home/sina/Projects/introduction-to-algorithm-phase-1-c/output.out");

    for (auto &grid : all_nodes) {
        outfile << grid -> number << ": " << grid -> capital.number << std::endl;
    }


    outfile.close();

    std::cout << "Total Time: " << (float) (clock() - tStart) / CLOCKS_PER_SEC << std::endl;

    return 0;
}