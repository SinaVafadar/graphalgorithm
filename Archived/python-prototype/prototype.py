import heapq
import math

minimum_spanning_tree = []

Q = []
isolated_nodes = []
fragment_nodes = []
for counter in range(10000):
    isolated_nodes.append((counter, counter))

top_node = isolated_nodes[0]
isolated_nodes.remove(top_node)
fragment_nodes.append(top_node)

global_distance = math.inf
distance, nearest_node = math.inf, None
for node in isolated_nodes:
    distance = (node[0] - top_node[0]) ** 2
    distance += (node[1] - top_node[1]) ** 2
    if distance < global_distance:
        nearest_node = node
        global_distance = distance

heapq.heappush(Q, (distance, [top_node, nearest_node]))

while len(minimum_spanning_tree) != 9999:
    top_node = Q[0][1]
    while top_node[1] in fragment_nodes:
        top_node = heapq.heappop(Q)
        distance, nearest_node = math.inf, None
        global_distance = math.inf
        for node in isolated_nodes:
            distance = (node[0] - top_node[0]) ** 2
            distance += (node[1] - top_node[1]) ** 2
            if distance < global_distance:
                nearest_node = node
                global_distance = distance
        heapq.heappush(Q, (distance, [top_node, nearest_node]))
        top_node = nearest_node
    top_node = heapq.heappop(Q)[1]
    minimum_spanning_tree.append((top_node[0], top_node[1]))
    isolated_nodes.remove(top_node[1])
    fragment_nodes.append(top_node[1])
    distance, nearest_node = math.inf, None
    global_distance = math.inf
    top_node = top_node[1]
    for node in isolated_nodes:
        distance = (node[0] - top_node[0]) ** 2
        distance += (node[1] - top_node[1]) ** 2
        if distance < global_distance:
            nearest_node = node
            global_distance = distance
    heapq.heappush(Q, (distance, [top_node, nearest_node]))

print(minimum_spanning_tree)