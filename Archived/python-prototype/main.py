import numpy as np
from sklearn.neighbors import KDTree
import heapq
import math

# A list for saving edges:
MST_edges = []
q = []
fragment_nodes = np.zeros(shape=(1, 2))

# Create a kd tree:
# range_points = np.random.RandomState(0)
# points = range_points.random_sample((10, 2))
points = np.zeros(shape=(10, 2))
for counter in range(10):
    points[counter][0] = counter
    points[counter][1] = counter

# select the first node and find the nearest neighbor:
fragment_nodes[0] = np.array(points[0])
points = np.delete(points, 0, axis=0)
tree_points = KDTree(points, leaf_size=2)
distance, index = tree_points.query([points[0]], k=2)

# Establish link to the nearest neighbor:
MST_edges.append((fragment_nodes[0], points[index[-1][-1]]))

# enter node and its (real) priority into priority queue:
heapq.heappush(q, [distance[-1][-1], [fragment_nodes[-1], points[index[-1][-1]]]])
fragment_nodes = np.vstack((np.array(fragment_nodes), np.array(points[index[-1][-1]])))
points = np.delete(points, index[-1][-1], axis=0)
tree_points = KDTree(points, leaf_size=2)

# loop until MST is complete:
while len(MST_edges) != 9:
    top_node = q[0][1]
    while top_node[1] in fragment_nodes:
        whole_top_node = heapq.heappop(q)
        top_node = whole_top_node[1]
        this_distance = math.inf
        for node in points:
            distance = (node[0] - top_node[1][0]) ** 2
            distance += (node[1] - top_node[1][1]) ** 2
            if distance < this_distance:
                top_node[1] = node
                this_distance = distance
        whole_top_node[0] = this_distance
        whole_top_node[1][1] = top_node[1]
        heapq.heappush(q, whole_top_node)
    top_node = heapq.heappop(q)[1]
    MST_edges.append((top_node[0], top_node[1]))
    if len(points) >= 2:
        distance, index = tree_points.query([top_node[1]], k=2)
    else:
        distance, index = tree_points.query([top_node[1]], k=1)
    heapq.heappush(q, [distance[-1][-1], [top_node[1], points[index[-1][-1]]]])
    points = np.delete(points, index[-1][-1], axis=0)
    if len(points) > 0:
        tree_points = KDTree(points, leaf_size=2)

print(MST_edges)
print(len(MST_edges))