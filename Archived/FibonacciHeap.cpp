#include <iostream>
#include <algorithm>
#include <vector>
#include <bits/stdc++.h>

using namespace std;

struct node {
    struct node* left;
    struct node* right;
    struct node* child;
    struct node* parent;
    int key;
    int degree;
    char mark;
    char c;
};

struct node* minimum_node = nullptr;
int number_of_nodes = 0;

void insert(int value) {
    struct node* new_node = (struct node*) malloc(sizeof(struct node));
    new_node -> key = value;
    new_node -> degree = 0;
    new_node -> mark = 'W';
    new_node -> c = 'N';
    new_node -> parent = nullptr;
    new_node -> child = nullptr;
    new_node -> left = new_node;
    new_node -> right = new_node;
    if (minimum_node != nullptr) {
        (minimum_node -> left) -> right = new_node;
        new_node -> right = minimum_node;
        new_node -> left = minimum_node -> left;
        minimum_node -> left = new_node;
        if (new_node -> key < minimum_node -> key) {
            minimum_node = new_node;
        }
    } else {
        minimum_node = new_node;
    }
    number_of_nodes += 1;
};

void fibonacci_link(struct node* node_pointer_two, struct node* node_pointer_one) {
    (node_pointer_two -> left) -> right = node_pointer_two -> right;
    (node_pointer_two -> right) -> left = node_pointer_two -> left;
    if (node_pointer_one -> right == node_pointer_one)
        minimum_node = node_pointer_one;
    node_pointer_two -> left = node_pointer_two;
    node_pointer_two -> right = node_pointer_two;
    node_pointer_two -> parent = node_pointer_one;
    if (node_pointer_one -> child == nullptr)
        node_pointer_one -> child = node_pointer_two;
    node_pointer_two -> right = node_pointer_one -> child;
    node_pointer_two -> left = (node_pointer_one -> child) -> left;
    ((node_pointer_one -> child) -> left) -> right = node_pointer_two;
    (node_pointer_one -> child) -> left = node_pointer_two;
    if (node_pointer_two -> key < (node_pointer_one -> child) -> key)
        node_pointer_one -> child = node_pointer_two;
    node_pointer_one -> degree += 1;
};

void consolidate() {
    int temp_one;
    float temp_two = (log(number_of_nodes)) / (log(2));
    int temp_three = temp_two;
    struct node* array[temp_three];
    for (int counter = 0; counter <= temp_three; counter += 1)
        array[counter] = nullptr;
    node* node_pointer_one = minimum_node;
    node* node_pointer_two;
    node* node_pointer_three;
    node* node_pointer_four = node_pointer_one;
    do {
        node_pointer_four = node_pointer_four -> right;
        temp_one = node_pointer_one -> degree;
        while (array[temp_one] != nullptr) {
            node_pointer_two = array[temp_one];
            if (node_pointer_one -> key > node_pointer_two -> key) {
                node_pointer_three = node_pointer_one;
                node_pointer_one = node_pointer_two;
                node_pointer_two = node_pointer_three;
            }
            if (node_pointer_two == minimum_node)
                minimum_node = node_pointer_one;
            fibonacci_link(node_pointer_two, node_pointer_one);
            if (node_pointer_one -> right == node_pointer_one)
                minimum_node = node_pointer_one;
            array[temp_one] = nullptr;
            temp_one += 1;
        }
        array[temp_one] = node_pointer_one;
        node_pointer_one = node_pointer_one->right;
    } while (node_pointer_one != minimum_node);
    minimum_node = nullptr;
    for (int counter = 0; counter <= temp_three; counter++) {
        if (array[counter] != nullptr) {
            array[counter] -> left = array[counter];
            array[counter] -> right = array[counter];
            if (minimum_node != nullptr) {
                (minimum_node -> left) -> right = array[counter];
                array[counter] -> right = minimum_node;
                array[counter] -> left = minimum_node -> left;
                minimum_node -> left = array[counter];
                if (array[counter] -> key < minimum_node -> key)
                    minimum_node = array[counter];
            } else {
                minimum_node = array[counter];
            }
            if (minimum_node == nullptr)
                minimum_node = array[counter];
            else if (array[counter] -> key < minimum_node -> key)
                minimum_node = array[counter];
        }
    }
};

void extract_minimum() {
    if (minimum_node == nullptr) {
        cout << "The heap is empty" << endl;
    } else {
        node* temp = minimum_node;
        node* node_pointer;
        node* temp_child;
        if (temp -> child != nullptr) {
            temp_child = temp -> child;
            do {
                node_pointer = temp_child -> right;
                (minimum_node -> left) -> right = temp_child;
                temp_child -> right = minimum_node;
                temp_child -> left = minimum_node -> left;
                minimum_node -> left = temp_child;
                if (temp_child -> key < minimum_node -> key)
                    minimum_node = temp_child;
                temp_child -> parent = nullptr;
                temp_child = node_pointer;
            } while (node_pointer != temp -> child);
        }
        (temp -> left) -> right = temp -> right;
        (temp -> right) -> left = temp -> left;
        minimum_node = temp -> right;
        if (temp == temp -> right && temp -> child == nullptr) {
            minimum_node = nullptr;
        } else {
            minimum_node = temp -> right;
            consolidate();
        }
        number_of_nodes -= 1;
    }
}

void cut(struct node* found, struct node* temp) {
    if (found == found -> right)
        temp -> child = nullptr;
    (found -> left) -> right = found -> right;
    (found -> right) -> left = found -> left;
    if (found == temp -> child)
        temp -> child = found -> right;
    temp -> degree = temp -> degree - 1;
    found -> right = found;
    found -> left = found;
    (minimum_node -> left) -> right = found;
    found -> right = minimum_node;
    found -> left = minimum_node -> left;
    minimum_node -> left = found;
    found -> parent = nullptr;
    found -> mark = 'B';
}

void cascade_cut(struct node* input_node) {
    node* node_pointer = input_node -> parent;
    if (node_pointer != nullptr) {
        if (input_node -> mark == 'W') {
            input_node -> mark = 'B';
        } else {
            cut(input_node, node_pointer);
            cascade_cut(node_pointer);
        }
    }
}

void decrease_key(struct node* found, int value) {
    if (minimum_node == nullptr)
        cout << "The Heap is Empty" << endl;
    if (found == nullptr)
        cout << "Node not found in the Heap" << endl;
    else {
        found->key = value;
        struct node *found_parent = found->parent;
        if (found_parent != nullptr && found->key < found_parent->key) {
            cut(found, found_parent);
            cascade_cut(found_parent);
        }
        if (found->key < minimum_node->key)
            minimum_node = found;
    }
}

void find_and_decrease(struct node* minimum_node, int old_value, int new_value) {
    struct node* found;
    node* temp_node = minimum_node;
    temp_node -> c = 'Y';
    node* found_node = nullptr;
    if (temp_node -> key == old_value) {
        found_node = temp_node;
        temp_node -> c = 'N';
        found = found_node;
        decrease_key(found, new_value);
    }
    if (found_node == nullptr) {
        if (temp_node -> child != nullptr)
            find_and_decrease(temp_node -> child, old_value, new_value);
        if ((temp_node -> right) -> c != 'Y')
            find_and_decrease(temp_node -> right, old_value, new_value);
    }
    temp_node -> c = 'N';
}

void deletion(int value) {
    if (minimum_node == nullptr)
        cout << "The heap is empty" << endl;
    else {
        find_and_decrease(minimum_node, value, 0);
        extract_minimum();
        cout << "Key Deleted" << endl;
    }
}

void display() {
    node* node_pointer = minimum_node;
    if (node_pointer == nullptr)
        cout << "The Heap is Empty" << endl;
    else {
        cout << "The root nodes of Heap are: " << endl;
        do {
            cout << node_pointer -> key;
            node_pointer = node_pointer -> right;
            if (node_pointer != minimum_node) {
                cout << "-->";
            }
        } while (node_pointer != minimum_node && node_pointer -> right != nullptr);
        cout << endl << "The heap has " << number_of_nodes << " nodes" << endl << endl;
    }
}

int main() {
    insert(2);
    insert(3);
    insert(1);
    insert(5);
    display();
    extract_minimum();
    display();
    return 0;
}