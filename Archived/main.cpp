#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <ctime>
#include <fstream>

using namespace std;

vector<vector<struct Node>> grid_nodes(2500);
vector<pair<float, struct Node>> queue;

int calculate_grid_number(float x, float y) {
    int row_number = (int) (((int) (x * 100) + 50) / 2);
    int column_number = (int) (((int) (y * 100) + 50) / 2);
    return row_number * 50 + column_number;
}

struct Node {
    pair<float, float> coordinate;
    int grid_number{};
    int node_number{};
    int state{};
    bool is_isolated = true;
};

float find_distance(float x1, float y1, float x2, float y2) {
    return (float) sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

bool greater_heap(pair<float, struct Node> first, pair<float, struct Node> second) {
    return first.first > second.first;
}

pair<float, struct Node> find_nearest_neighbour(struct Node source_node) {
    int grid_number = source_node.grid_number;
    struct Node nearest_node;
    float global_distance = numeric_limits<float>::infinity();
    float this_distance;
    for (int number : {51, 50, 49, 1, 0, -1, -49, -50, -51}) {
        if (grid_number - number >= 0 && grid_number + number <= grid_nodes.size() && !grid_nodes[number].empty()) {
            for (auto &node : grid_nodes[grid_number - number]) {
                this_distance = find_distance(node.coordinate.first, node.coordinate.second,
                                              source_node.coordinate.first, source_node.coordinate.second);
                if (this_distance < global_distance) {
                    nearest_node = node;
                    global_distance = this_distance;
                }
            }
        }
    }
    return make_pair(global_distance, nearest_node);
}

int main() {
    float x, y;
    int count;
    ifstream infile("/home/sina/Projects/graphalgorithm/data.txt");
    infile >> count;

    for (int number = 0; number < count; number += 1) {
        struct Node new_node;
        infile >> x;
        infile >> y;
        new_node.coordinate = make_pair(x, y);
        infile >> new_node.state;
        new_node.node_number = number;
        new_node.grid_number = calculate_grid_number(x, y);
        grid_nodes[new_node.grid_number].emplace_back(new_node);
        cout << grid_nodes[new_node.grid_number].size() << endl;
    }

    int number = 0;
    while (grid_nodes[number].empty()) number += 1;
    struct Node top_node = grid_nodes[number][0];
    top_node.is_isolated = false;
    auto iterator = grid_nodes[0].begin();
    grid_nodes[0].erase(iterator);

    pair<float, struct Node> nearest_node_with_distance = find_nearest_neighbour(top_node);

    cout << grid_nodes[0].size() << endl;

//    for (int counter = 0; counter < 2500; counter += 1) {
//        cout << grid_nodes[counter].size() << endl;
//    }

//    queue.emplace_back(nearest_node_with_distance);
//    make_heap(queue.begin(), queue.end(), greater_heap);

//    for (int i = 0; i < count; i += 1) {
//        cout << "Coordinate: " << isolated_nodes[i].coordinate.first << " " << isolated_nodes[i].coordinate.second
//             << endl;
//        cout << "Grid Number: " << isolated_nodes[i].grid_number << endl;
//        cout << "Node Number: " << isolated_nodes[i].node_number << endl;
//        cout << "State: " << isolated_nodes[i].state << endl;
//        cout << "Isolation: " << isolated_nodes[i].is_isolated << endl;
//
//    }

    return 0;
}