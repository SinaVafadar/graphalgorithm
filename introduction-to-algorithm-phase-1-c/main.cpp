#include <cmath>
#include <vector>
#include <fstream>
#include <iostream>
#include <limits>
#include <algorithm>

#define FLOAT_MIN -10000000
#define FLOAT_MAX +10000000

std::vector<struct Grid *> all_nodes;
std::vector<int> grid_numbers;
std::vector<float> distances;

struct Node {
    float coordinate[2]{};
    int state{};
    int number;

    Node(float x, float y, int state, int number) {
        this -> coordinate[0] = x;
        this -> coordinate[1] = y;
        this -> state = state;
        this -> number = number;
    }
};

struct Grid {
    int number;
    std::vector<struct Node> nodes;
    struct Node capital = Node(0, 0, 0, -1);

    explicit Grid(int number) {
        this -> number = number;
    }
};

float find_max_distances(float x, float y, int count, int grid_number) {
    float maximum_distance = FLOAT_MIN;
    for (int counter = 0; counter <= count; counter += 1) {
        float x_difference = all_nodes[grid_number] -> nodes[counter].coordinate[0] - x;
        float y_difference = all_nodes[grid_number] -> nodes[counter].coordinate[1] - y;
        auto this_distance = (float) sqrt(pow(x_difference, 2) + pow(y_difference, 2));
        if (this_distance > maximum_distance) maximum_distance = this_distance;
    }
    return maximum_distance;
}

int main() {

    clock_t tStart = clock();

    float x;
    float y;
    int state;
    int count;
    struct Grid *new_grid = nullptr;
    std::ifstream infile("/home/sina/Projects/introduction-to-algorithm-phase-1-c/dataset4.txt");
    infile >> count;
    for (int counter = 0; counter < count; counter += 1) {
        infile >> x;
        infile >> y;
        infile >> state;
        grid_numbers.emplace_back(state);
    }

    sort(grid_numbers.begin(), grid_numbers.end());
    grid_numbers.erase(std::unique(grid_numbers.begin(), grid_numbers.end()), grid_numbers.end());
    infile.clear();
    infile.seekg(0, std::ios::beg);

    infile >> count;
    for (int counter = 0; counter < grid_numbers.size(); counter += 1) {
        new_grid = new Grid(counter);
        all_nodes.emplace_back(new_grid);
    }

    for (int counter = 0; counter < count; counter += 1) {
        infile >> x;
        infile >> y;
        infile >> state;
        struct Node new_node = Node(x, y, state, counter + 1);
        new_grid = new Grid(state);
        if (!all_nodes[state - 1] -> nodes.empty()) {
            new_grid = all_nodes[state - 1];
            new_grid -> nodes.emplace_back(new_node);
        } else {
            new_grid -> nodes.emplace_back(new_node);
            new_grid -> capital.state = state;
            new_grid -> capital.coordinate[0] = x;
            new_grid -> capital.coordinate[1] = y;
            all_nodes[state - 1] = new_grid;
        }
    }

    for (auto &grid : all_nodes) {
        float minimax = FLOAT_MAX;
        auto *answer_node = new Node(0, 0, grid -> number, 0);
        for (auto &node : grid -> nodes) {
            float this_distance = find_max_distances(node.coordinate[0], node.coordinate[1], grid -> nodes.size(), node.state - 1);
            if (minimax > this_distance) {
                *answer_node = node;
                minimax = this_distance;
            }
        }
        grid -> capital.coordinate[0] = answer_node -> coordinate[0];
        grid -> capital.coordinate[1] = answer_node -> coordinate[1];
        grid -> capital.number = answer_node->number;
    }

    for (auto &grid : all_nodes) {
        std::cout << grid -> number << ": " << grid -> capital.number << std::endl;
    }

    std::ofstream outfile;
    outfile.open("/home/sina/Projects/introduction-to-algorithm-phase-1-c/output.out");

    for (auto &grid : all_nodes) {
        outfile << grid -> number << ": " << grid -> capital.number << std::endl;
    }

    outfile.close();

    std::cout << "Total Time: " << (float) (clock() - tStart) / CLOCKS_PER_SEC << std::endl;

    return 0;
}
