#include <cmath>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>


#define FLOAT_MIN -10000000
#define FLOAT_MAX +10000000

std::vector<struct Grid *> all_nodes;
std::vector<int> grid_numbers;
std::vector<struct Node *> global_nodes;
std::vector<struct Edge> edges_queue;
std::vector<struct Edge> MST;
std::vector<int> number_of_edges;
float global_distance;
struct Node *nearest_neighbour = nullptr;
struct Node *minimum_father = nullptr;
struct Node *main_tree = nullptr;
struct Node **root_of_tree = &main_tree;

struct Node {
    float coordinate[2]{};
    int state{};
    int number;
    struct Node *left = nullptr;
    struct Node *right = nullptr;
    bool is_fragmented = false;

    Node(float x, float y, int state, int number) {
        this -> coordinate[0] = x;
        this -> coordinate[1] = y;
        this -> state = state;
        this -> number = number;
    }
};

struct Grid {
    int number;
    std::vector<struct Node> nodes;
    struct Node capital = Node(0, 0, 0, -1);

    explicit Grid(int number) {
        this -> number = number;
    }
};

struct Edge {
    struct Node *source = nullptr;
    struct Node *destination = nullptr;
    float distance;

    Edge(struct Node *source, struct Node *destination, float distance) {
        this  ->  source = source;
        this  ->  destination = destination;
        this  ->  distance = distance;
    }
};

float find_distance(float x1, float y1, float x2, float y2) {
    return (float) sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

bool compare(const struct Edge first, const struct Edge second) {
    return first.distance >= second.distance;
}

void insert_node(struct Node **root, struct Node *node, int depth) {
    if (*root == nullptr) {*root = node; return ;}

    int dimension = depth % 2;
    if (node -> coordinate[dimension] < (*root) -> coordinate[dimension]) {
        insert_node(&(*root) -> left, node, depth + 1);
    } else {
        insert_node(&(*root) -> right, node, depth + 1);
    }
}

void nearest_neighbour_search(struct Node *root, struct Node *node, int depth) {
    if (root == nullptr) return ;

    float distance_from_root = find_distance(node -> coordinate[0], node -> coordinate[1],
                                             root -> coordinate[0], root -> coordinate[1]);
    if (distance_from_root < global_distance) {
        nearest_neighbour = root;
        global_distance = distance_from_root;
    }

    int dimension = depth % 2;
    if (node -> coordinate[dimension] < root -> coordinate[dimension]) {
        if (root -> left != nullptr) nearest_neighbour_search(root -> left, node, depth + 1);
    } else {
        if (root -> right != nullptr) nearest_neighbour_search(root -> right, node, depth + 1);
    }

    float worse_side_distance = std::abs(root -> coordinate[dimension] - node -> coordinate[dimension]);
    if (worse_side_distance < global_distance) {
        if (node -> coordinate[dimension] < root -> coordinate[dimension]) {
            if (root -> right != nullptr) nearest_neighbour_search(root -> right, node, depth + 1);
        } else {
            if (root -> left != nullptr) nearest_neighbour_search(root -> left, node, depth + 1);
        }
    }
}


std::pair<struct Node *, struct Node *> find_minimum(struct Node *root, int dimension_of_minimum, int depth, struct Node *father) {
    if (root == nullptr) return std::make_pair(nullptr, nullptr);

    int dimension = depth % 2;
    if (dimension == dimension_of_minimum) {
        if (root -> left == nullptr) return std::make_pair(root, father);
        return find_minimum(root -> left, dimension_of_minimum, depth + 1, root);
    }

    std::pair<struct Node *, struct Node *> left_pair = find_minimum(root -> left, dimension_of_minimum, depth + 1, root);
    struct Node *left = left_pair.first;
    std::pair<struct Node *, struct Node *> right_pair = find_minimum(root -> right, dimension_of_minimum, depth + 1, root);
    struct Node *right = right_pair.first;

    std::pair<struct Node *, struct Node *> temp_pair = right_pair;
    struct Node *temp_node;
    if (left != nullptr && right != nullptr) {
        if (left -> coordinate[dimension_of_minimum] < right -> coordinate[dimension_of_minimum]) temp_pair = left_pair;
    } else if (left == nullptr && right != nullptr) {
        temp_pair = right_pair;
    } else if (left != nullptr) {
        temp_pair = left_pair;
    }
    temp_node = temp_pair.first;

    std::pair<struct Node *, struct Node *> answer_pair = std::make_pair(root, father);
    struct Node *answer = root;
    if (temp_node != nullptr && temp_node -> coordinate[dimension_of_minimum] < answer -> coordinate[dimension_of_minimum]) {
        minimum_father = root;
        answer_pair = temp_pair;
    }

    return answer_pair;
}


struct Node *delete_node(struct Node *root, struct Node *node, int depth, struct Node *father) {
    if (root == nullptr) return root;

    int dimension = depth % 2;
    struct Node *minimum_node;
    std::pair<struct Node *, struct Node *> minimum_pair;
    if (root == node) {
        if (root -> right != nullptr) {
            minimum_pair = find_minimum(root -> right, dimension, depth + 1, root);
            minimum_node = minimum_pair.first;
            minimum_father = minimum_pair.second;

            if (node == *root_of_tree) *root_of_tree = minimum_node;

            auto *temp = (struct Node *) malloc(sizeof(struct Node));
            *temp = Node(minimum_node -> coordinate[0], minimum_node -> coordinate[1], minimum_node -> state, 0);
            temp -> right = minimum_node -> right;
            temp -> left = minimum_node -> left;

            if (minimum_father == nullptr) {
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> left;
                minimum_node -> right = root -> right;
                root -> right = nullptr;
                root -> left = nullptr;

            } else {
                if (minimum_father -> left == minimum_node) minimum_father -> left = temp;
                else if (minimum_father -> right == minimum_node) minimum_father -> right = temp;
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> left;
                minimum_node -> right = root -> right;
                root -> right = nullptr;
                root -> left = nullptr;

            }
            delete_node(minimum_node -> right, temp, depth + 1, minimum_node);

        } else if (root -> left != nullptr) {
            minimum_pair = find_minimum(root -> left, dimension, depth + 1, root);
            minimum_node = minimum_pair.first;
            minimum_father = minimum_pair.second;

            if (node == *root_of_tree) *root_of_tree = minimum_node;

            auto *temp = (struct Node *) malloc(sizeof(struct Node));
            *temp = Node(minimum_node -> coordinate[0], minimum_node -> coordinate[1], minimum_node -> state, 0);
            temp -> right = minimum_node -> right;
            temp -> left = minimum_node -> left;

            if (minimum_father == nullptr) {
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> right;
                minimum_node -> right = root -> left;
                root -> right = nullptr;
                root -> left = nullptr;
            } else {
                if (minimum_father -> left == minimum_node) minimum_father -> left = temp;
                else if (minimum_father -> right == minimum_node) minimum_father -> right = temp;
                if (father != nullptr) {
                    if (father -> left == root) father -> left = minimum_node;
                    else if (father -> right == root) father -> right = minimum_node;
                }
                minimum_node -> left = root -> right;
                minimum_node -> right = root -> left;
                root -> right = nullptr;
                root -> left = nullptr;
            }
            delete_node(minimum_node -> right, temp, depth + 1, minimum_node);
        } else {
            if (father != nullptr) {
                if (father -> left == node) father -> left = nullptr;
                else if (father -> right == node) father -> right = nullptr;
            }
            return nullptr;
        }

        return minimum_node;
    }

    if (node -> coordinate[dimension] < root -> coordinate[dimension]) delete_node(root -> left, node, depth + 1, root);
    else delete_node(root -> right, node, depth + 1, root);

    return root;
}

float find_max_distances(float x, float y, int count, int grid_number) {
    float maximum_distance = FLOAT_MIN;
    for (int counter = 0; counter <= count; counter += 1) {
        float x_difference = all_nodes[grid_number] -> nodes[counter].coordinate[0] - x;
        float y_difference = all_nodes[grid_number] -> nodes[counter].coordinate[1] - y;
        auto this_distance = (float) sqrt(pow(x_difference, 2) + pow(y_difference, 2));
        if (this_distance > maximum_distance) maximum_distance = this_distance;
    }
    return maximum_distance;
}

int main() {

    clock_t tStart = clock();

    float x;
    float y;
    int state;
    int count;
    struct Grid *new_grid = nullptr;
    std::ifstream infile("/home/sina/Projects/introduction-to-algorithm-phase-3/dataset2.txt");
    infile >> count;
    for (int counter = 0; counter < count; counter += 1) {
        infile >> x;
        infile >> y;
        infile >> state;
        grid_numbers.emplace_back(state);
        auto *new_node = new Node(x, y, state, counter + 1);
        global_nodes.emplace_back(new_node);
        number_of_edges.emplace_back(0);
    }

    sort(grid_numbers.begin(), grid_numbers.end());
    grid_numbers.erase(std::unique(grid_numbers.begin(), grid_numbers.end()), grid_numbers.end());
    infile.clear();
    infile.seekg(0, std::ios::beg);

    infile >> count;
    for (int counter = 0; counter < grid_numbers.size(); counter += 1) {
        new_grid = new Grid(counter);
        all_nodes.emplace_back(new_grid);
    }

    for (int counter = 0; counter < count; counter += 1) {
        infile >> x;
        infile >> y;
        infile >> state;
        struct Node new_node = Node(x, y, state, counter + 1);
        new_grid = new Grid(state);
        if (!all_nodes[state - 1] -> nodes.empty()) {
            new_grid = all_nodes[state - 1];
            new_grid -> nodes.emplace_back(new_node);
        } else {
            new_grid -> nodes.emplace_back(new_node);
            new_grid -> capital.state = state;
            new_grid -> capital.coordinate[0] = x;
            new_grid -> capital.coordinate[1] = y;
            all_nodes[state - 1] = new_grid;
        }
    }

    for (auto &grid : all_nodes) {
        float minimax = FLOAT_MAX;
        auto *answer_node = new Node(0, 0, grid -> number, 0);
        for (auto &node : grid -> nodes) {
            float this_distance = find_max_distances(node.coordinate[0], node.coordinate[1], grid -> nodes.size(), node.state - 1);
            if (minimax > this_distance) {
                *answer_node = node;
                minimax = this_distance;
            }
        }
        grid -> capital.coordinate[0] = answer_node -> coordinate[0];
        grid -> capital.coordinate[1] = answer_node -> coordinate[1];
        grid -> capital.number = answer_node -> number;
    }

    auto *first_node = global_nodes[0];
    first_node -> is_fragmented = true;
    first_node -> number = 1;

    for (int counter = 0; counter < count - 1; counter += 1) {
        auto *new_node = global_nodes[counter + 1];
        insert_node(&main_tree, new_node, 0);
    }

    global_distance = std::numeric_limits<float>::infinity();
    nearest_neighbour_search(main_tree, first_node, 0);
    struct Edge new_edge = Edge(first_node, nearest_neighbour, global_distance);
    edges_queue.push_back(new_edge);
    make_heap(edges_queue.begin(), edges_queue.end(), compare);

    while (MST.size() != count - 1) {
        struct Edge candidate_edge = edges_queue.front();
        while(candidate_edge.destination -> is_fragmented) {
            struct Node *source_node = candidate_edge.source;
            pop_heap(edges_queue.begin(), edges_queue.end(), compare);
            edges_queue.pop_back();
            global_distance = std::numeric_limits<float>::infinity();
            nearest_neighbour_search(main_tree, source_node, 0);
            new_edge = Edge(source_node, nearest_neighbour, global_distance);
            edges_queue.emplace_back(new_edge);
            push_heap(edges_queue.begin(), edges_queue.end(), compare);
            candidate_edge = edges_queue.front();
        }
        struct Node *new_node_in_fragment = candidate_edge.destination;
        new_node_in_fragment -> is_fragmented = true;
        MST.push_back(candidate_edge);
        number_of_edges[candidate_edge.source -> number - 1] += 1;
        number_of_edges[candidate_edge.destination -> number - 1] += 1;
        delete_node(main_tree, new_node_in_fragment, 0, nullptr);
        global_distance = std::numeric_limits<float>::infinity();
        nearest_neighbour_search(main_tree, new_node_in_fragment, 0);
        new_edge = Edge(new_node_in_fragment, nearest_neighbour, global_distance);
        edges_queue.emplace_back(new_edge);
        push_heap(edges_queue.begin(), edges_queue.end(), compare);
    }

    bool flag = true;
    if (all_nodes.size() == 1) {
        MST.clear();
    } else {
        while (flag) {
            flag = false;
            for (int edge_counter = 0; edge_counter < MST.size(); edge_counter += 1) {
                struct Edge edge = MST[edge_counter];
                if (number_of_edges[edge.destination->number - 1] == 1) {
                    if (all_nodes[edge.destination->state - 1]->capital.number != edge.destination->number) {
                        MST.erase(MST.begin() + edge_counter);
                        edge_counter -= 1;
                        number_of_edges[edge.source->number - 1] -= 1;
                        number_of_edges[edge.destination->number - 1] -= 1;
                        flag = true;
                    }
                } else if (number_of_edges[edge.source->number - 1] == 1) {
                    if (all_nodes[edge.source->state - 1]->capital.number != edge.source->number) {
                        MST.erase(MST.begin() + edge_counter);
                        edge_counter -= 1;
                        number_of_edges[edge.source->number - 1] -= 1;
                        number_of_edges[edge.destination->number - 1] -= 1;
                        flag = true;
                    }
                }
            }
        }
    }

    for (int counter = 0; counter < count; counter += 1) {
        if (number_of_edges[counter] == 2 && all_nodes[global_nodes[counter] -> state - 1] -> capital.number != global_nodes[counter] -> number) {
            int edge_number = 0;
            auto *middle_node = (struct Node *) malloc(sizeof(struct Node));
            struct Node *final_node;
            for (int edge_counter = 0; edge_counter < MST.size(); edge_counter += 1) {
                struct Edge edge = MST[edge_counter];
                if (edge.source -> number == counter + 1 || edge.destination -> number == counter + 1) {
                    if (edge_number == 0) {
                        middle_node = edge.source -> number == counter + 1 ? edge.destination : edge.source;
                        MST.erase(MST.begin() + edge_counter);
                        edge_counter -= 1;
                        edge_number = 1;
                    } else {
                        final_node = edge.source -> number == counter + 1 ? edge.destination : edge.source;
                        float this_distance = find_distance(middle_node->coordinate[0], middle_node->coordinate[1], final_node->coordinate[0], final_node->coordinate[1]);
                        struct Edge this_edge = Edge(final_node, middle_node, this_distance);
                        MST.erase(MST.begin() + edge_counter);
                        MST.emplace_back(this_edge);
                        edge_counter -= 1;
                        edge_number = 0;
                    }
                }
            }
        }
    }

    std::cout << "Number of Edges: " << MST.size() << std::endl;
    float total_distance = 0;
    for (auto &edge : MST) total_distance += edge.distance;
    std::cout << "Total Distance: " << total_distance << std::endl;
    std::cout << "Total Time: " << (float) (clock() - tStart) / CLOCKS_PER_SEC << std::endl;

    return 0;
}
