cmake_minimum_required(VERSION 3.16)
project(introduction_to_algorithm_phase_1)

set(CMAKE_CXX_STANDARD 14)

add_executable(introduction_to_algorithm_phase_1 main.cpp)